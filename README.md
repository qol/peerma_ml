# PeerMA_mL

## Purpose

Public repository to share supplementary research materials.

## Contents 

Supplemental materials regarding smartphone sensors derived features for a *Peers Know You: A Feasibility Study of the Predictive Value of Frequent Peer’s Observations to Estimate Human States*

The following files are available in this repository :
- sample_pDFs : shows an illustrative day of PeerMA derived features (pDFs)
- sample_sDFs : shows an illustrative day of smartphone-sensors derived features (sDFs)
- 
